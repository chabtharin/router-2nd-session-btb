import logo from "./logo.svg";
import "./App.css";
import { Route, Routes, useNavigate } from "react-router-dom";
import HomeComponent from "./components/HomeComponent";
import NavbarComponent from "./components/NavbarComponent";
import AboutUsComponent from "./components/AboutUsComponent";
import UserComponent from "./components/UserComponent";
import UserDetailComponent from "./components/UserDetailComponent";
import Account from "./components/Account";
import AccountProfile from "./components/AccountProfile";
import AccountDetail from "./components/AccountDetail";
import { useEffect, useState } from "react";
import ProtectedRoute from "./ProtectedRoute";
import LoginComponent from "./LoginComponent";

function App() {
  const [auth, setAuth] = useState(false);
  const navigate = useNavigate()

  const login = () => {
    setAuth(true);
  };

  useEffect(() => {
    auth ? navigate('/user') : navigate('/login')
  }, [auth]);
  const logout = () => {
    setAuth(false);
  };

  return (
    <div className="App">
      <NavbarComponent auth={auth} login={login} logout={logout}/>
      <Routes>
        <Route path="/" element={<HomeComponent />} />
        <Route path="about" element={<AboutUsComponent />} />
        {/* <Route path='user' element={<UserComponent/>}/> */}
        {/* <Route path='user/:id' element={<UserDetailComponent/>}/> */}
        <Route path="account" element={<Account />}>
          <Route path="profile" element={<AccountProfile />} />
          <Route path="detail" element={<AccountDetail />} />
        </Route>
        <Route
          path="/user"
          element={
            <ProtectedRoute auth={auth}>
              <UserComponent />
            </ProtectedRoute>
          }
        />
        <Route path="/login" element={<LoginComponent login={login} />} />
      </Routes>
    </div>
  );
}

export default App;
