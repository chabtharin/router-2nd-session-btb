import React from "react";
import { Button, Container } from "react-bootstrap";
import { Link, Outlet } from "react-router-dom";

export default function Account() {
  return (
    <div>
      <h1>Account</h1>
      <Button variant="success" as={Link} to="profile">
        Profile
      </Button>
      <Button variant="success" as={Link} to="detail">
        Detail
      </Button>
      <Container>
        <Outlet />
      </Container>
    </div>
  );
}
