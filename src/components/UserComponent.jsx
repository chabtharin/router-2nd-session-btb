import React from 'react'
import { Button } from 'react-bootstrap'
import { Link } from 'react-router-dom'

export default function UserComponent() {
  return (
    <div>
        <h1>UserComponent</h1>
        <Button as={Link} to='/user/12'>Go to detail</Button>
    </div>
  )
}
